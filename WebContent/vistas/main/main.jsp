<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>
<%@page import="java.util.List"%>
<%@ page import="com.develop.model.ProductoM" %>
<%@page import="com.develop.DAO.ProductoDAO"%>
<%@include file="/vistas/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<table  align="center" cellpadding="10px">
		<tr >
			<%ProductoDAO dao = new ProductoDAO();
				List<ProductoM> productosDB = dao.listarProducto();
				int cnt = 0;
				Iterator<ProductoM> it = productosDB.iterator();
				
				while(it.hasNext()){
					ProductoM p = it.next();
					String id = p.getId_producto()+"";
					String nombre = p.getNombre();
					String fabricante = p.getFabricante();
					String precio = p.getPrecio() + "";
					String existencias = p.getExistencias() + "";
					String url = p.getUrl()+"";
					if(cnt==3){
						out.print("<tr></tr>");
						cnt = 0;
					}
					cnt++;
				%>
							<td>
							<div class="card" style="width: 18rem;">
							  <img class="card-img-top" src='<%=url %>' height="300px" alt="Card image cap">
							 <div class="card-body">
							    <h5 class="card-title"><%=nombre%></h5>
							    <p class="card-text"><%=fabricante%><br>
													 Precio: $<%=precio%> pesos<br>
													 Existencias: <%=existencias%> paquetes<br></p>
							    <a href="vistas/main/verProducto.jsp?id_producto=<%=id%>" class="btn btn-primary">Ver</a>
							  </div>
							</div>
							</td>
							<%
						}%>
					</tr>
				</table>
</body>
</html>