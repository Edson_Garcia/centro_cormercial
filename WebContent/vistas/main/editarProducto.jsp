<%@page import="java.util.List"%>
<%@page import="com.develop.DAO.ProductoDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.develop.model.ProductoM" %>
<%@ page import="com.develop.DAO.UsuarioDAO" %>
<%@include file="/vistas/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="css/crud.css" rel="stylesheet"/>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	

			
			<div class="container2">
		<div class="table-responsive">
			<div class="table-wrapper">
				<div class="table-title2">
					<div class="row">
						<div class="col-xs-6">
							<h2>Administrar <b>Productos</b></h2>
						</div>
						<div class="col-xs-6">
							<a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Aggrega Nuevo Producto</span></a>						
						</div>
					</div>
				</div>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th >ID</th>
						<th>Nombre</th>
						<th>Fabricante</th>
						<th>Precio</th>
						<th>Existencias</th>
						<th>Fecha de Registro</th>
						<th>Editar/Eliminar</th>
						</tr>
					</thead>
			   <% ProductoDAO dao = new ProductoDAO();
				List<ProductoM> productosDB = dao.listarProducto();
				
				Iterator<ProductoM> it = productosDB.iterator();
				
				while(it.hasNext()){
					ProductoM p = it.next();
					int id = p.getId_producto();
					System.out.println(p.toString());
					String id_producto = p.getId_producto()+ "";
					String nombre = p.getNombre();
					String fabricante = p.getFabricante();
					String precio = p.getPrecio() + "";
					String existencias = p.getExistencias() + "";
					String fechaRegistroP = p.getFechaRegistroProducto() + "";
				%>
						<tr>
							<td><%=id_producto%></td>
							<td><%=nombre%></td>
							<td><%=fabricante%></td>
							<td><%=precio%></td>
							<td><%=existencias%></td>
							<td><%=fechaRegistroP%></td>
							<td>
							<a href="vistas/actualizarProducto/ActualizarProducto.jsp?id_producto=<%=id%>" ><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
							<form method="post" action="<%=basePath%>ProductoC?id_producto=<%=id_producto%>">							
								<button type="submit" name="button" value="EliminarP" class="material-icons">&#xE872;</button>
							</form>
							</td>
						</tr>
						<%}%>
				</table>
			</div>
		</div>        
    </div>
	<!-- Edit Modal HTML -->
	<div id="addEmployeeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form method="post" action="<%=basePath%>ProductoC">
					<div class="modal-header">						
						<h4 class="modal-title">Agregar Producto</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<label>Nombre</label>
							<input type="text" class="form-control" name="nombre" required>
						</div>
						<div class="form-group">
							<label>Fabricante</label>
							<input type="text" class="form-control" name="fabricante" required>
						</div>
						<div class="form-group">
							<label>Precio</label>
							<input type="text" class="form-control" name="precio" required>
						</div>
						<div class="form-group">
							<label>Existencias</label>
							<input type="text" class="form-control" name="existencias" required>
						</div>					
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<button type="submit" class="btn btn-success" name="button" id="button" value="GuardarP">Guardar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
<%@include file="/vistas/footer.jsp"%>
</html>