<%@page import="com.develop.model.ProductoM"%>
<%@page import="com.develop.DAO.ProductoDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
body{
	background: #F3F3F3;
}

 .container{
 	display: flex;
 	flex-direction: row;
 	margin: 0 auto;
 }
 .price{
 	border: none;
 	background: #fff;
 	width: 45px;
 }
 .exist{
 	border: none;
 	background: #fff;
 	width: 14px;
 }
 input {
	border: none;
 	background: #fff;
}
.product-block1{
	margin-top: 10%;
}
</style>
</head>
<body>
	<%ProductoDAO dao = new ProductoDAO(); 
			//int id = Integer.parseInt(request.getParameter("id_producto"));
			int id_producto = Integer.parseInt(request.getParameter("id_producto"));
			
			
			 ProductoM pm =  dao.seleccionarProducto(id_producto);
			String nombre = pm.getNombre();
			String fabricante = pm.getFabricante();
			String precio = pm.getPrecio()+"";
			String existencias = pm.getExistencias()+"";
			String url = pm.getUrl()+"";
			int existencia = pm.getExistencias();
			%>
			<div class="container">
					<div class="product-block">
						<img alt="" src='<%=url %>' width="600" height="600">
					</div>
					<div class="product-block1">
					<h1>Detalles del Producto</h1>
						<label>Nombre del producto:</label><input type="text" value="<%=nombre %>" disabled><br>
						<label>Fabricante: </label><input type="text" value="<%=fabricante %>" disabled><br>
						Precio: $<input type="text" value="<%=precio%>" class="price" disabled>pesos<br>
						<label>Existencias:</label> <input type="text" value="<%=existencias %>" class="exist" disabled>unidades<br>
						<label>Cantidad:</label><select class="selecc">
						<% for(int i = 1; i<= existencia;i++){%>
							<option value="<%=i%>"><%=i%></option>
			                <%}%>
						</select><hr>
					<button name="button" value="comprar" class="btn btn-primary">Comprar</button>	
					</div>
			</div>
</body>
<%@include file="/vistas/footer.jsp"%>
</html>