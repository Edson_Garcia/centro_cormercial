<%@page import="com.develop.model.VentaM"%>
<%@page import="com.develop.DAO.VentaDAO"%>
<%@page import="java.util.List"%>
<%@page import="com.develop.DAO.ProductoDAO"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="com.develop.model.ProductoM" %>
<%@ page import="com.develop.DAO.UsuarioDAO" %>
<%@include file="/vistas/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="css/crud.css" rel="stylesheet"/>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	

			
			<div class="container2">
		<div class="table-responsive">
			<div class="table-wrapper">
				<div class="table-title2">
					<div class="row">
						<div class="col-xs-6">
							<h2>Administrar <b>Productos</b></h2>
						</div>
						<div class="col-xs-6">
							<a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Aggrega Nuevo Producto</span></a>						
						</div>
					</div>
				</div>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
						<th >ID</th>
						<th>Usuario</th>
						<th>Producto</th>
						<th>Cantidad</th>
						<th>Precio</th>
						<th>Total</th>
						<th>Feca de compra</th>
						<th>Eliminar</th>
						</tr>
					</thead>
			   <% VentaDAO dao = new VentaDAO();
				List<VentaM> ventaDB = dao.listarVenta();
				
				Iterator<VentaM> it = ventaDB.iterator();
				
				while(it.hasNext()){
					VentaM v = it.next();
					int id_venta = v.getId_venta();
					String usuario = v.getFkusuario();
					String producto = v.getNombreP();
					String cantidad = v.getCantidadP()+"";
					String precio = v.getPrecioP()+"";
					String total = v.getTotalV()+"";
					String fechaRegistroP = v.getFechaRegistroVenta()+"";
				%>
						<tr>
							<td><%=id_venta%></td>
							<td><%=usuario%></td>
							<td><%=producto%></td>
							<td><%=cantidad%></td>
							<td><%=precio%></td>
							<td><%=total%></td>
							<td><%=fechaRegistroP%></td>
							<td>
							<form method="post" action="<%=basePath%>ProductoC?id_producto=<%=id_venta%>">							
								<button type="submit" name="button" value="EliminarP" class="material-icons">&#xE872;</button>
							</form>
							</td>
						</tr>
						<%}%>
				</table>
			</div>
		</div>        
    </div>
</body>
<%@include file="/vistas/footer.jsp"%>
</html>