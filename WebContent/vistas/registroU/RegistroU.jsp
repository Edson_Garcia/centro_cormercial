<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@include file="/vistas/header.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js">

<title>Insert title here</title>
<script>
body {
    color: #000;
    overflow-x: hidden;
    height: 100%;
    background-repeat: no-repeat;
    background-size: 100% 100%
}

.card {
	padding: 30px 40px;
	margin-top: 60px;
	margin-bottom: 60px;
	border: none !important;
	box-shadow: 0 6px 12px 0 rgba(0, 0, 0, 0.2)
}

.blue-text {
    color: #00BCD4
}

.form-control-label {
    margin-bottom: 0
}

input,
button {
    padding: 8px 15px;
    border-radius: 5px !important;
    margin: 5px 0px;
    box-sizing: border-box;
    border: 1px solid #ccc;
    font-size: 18px !important;
    font-weight: 300
}

input:focus{
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    border: 1px solid #00BCD4;
    outline-width: 0;
    font-weight: 400
}

.btn-block {
    text-transform: uppercase;
    font-size: 15px !important;
    font-weight: 400;
    height: 43px;
    cursor: pointer
}

.btn-block:hover {
    color: #fff !important
}

button:focus {
    -moz-box-shadow: none !important;
    -webkit-box-shadow: none !important;
    box-shadow: none !important;
    outline-width: 0
}
</script>
</head>
<body>
<div class="container-fluid px-1 py-5 mx-auto">
    <div class="row d-flex justify-content-center">
        <div class="col-xl-7 col-lg-8 col-md-9 col-11 text-center">
            <div class="card">
                <h5 class="text-center mb-4">Registro de Usuario</h5>
					<form  class="form-card"  action="<%=basePath%>UsuarioAltaC">
			            <div class="form-group row justify-content-center"><input type="text" placeholder="Nombre" name="nombre" ></div>
			            <div class="form-group row justify-content-center"><input type="text" placeholder="Apellido Paterno" name="apellidop" ></div>
			            <div class="form-group row justify-content-center"><input type="text" placeholder="Apellido Materno" name="apellidom" ></div>
			            <div class="form-group row justify-content-center"><input type="text" placeholder="Usuario(E.J. Edsono, Ed, etc)" name="usuario" ></div>
			            <div class="form-group row justify-content-center"><input type="text" placeholder="Correo" name="correo" ></div>
			            <div class="form-group row justify-content-center"><input type="text" placeholder="Password" name="password" id="fname"></div>
			            <div class="row justify-content-center">
			            <div class="form-group col-sm-6"><button type="submit" class="btn-block btn-primary">Registrar</button></div>
			            </div>
			        </form>
			        </div>
            </div>
        </div>
    </div>
<%@include file="/vistas/footer.jsp"%>
</body>
</html>