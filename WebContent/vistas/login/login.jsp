<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
@charset "ISO-8859-1";
*{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    }

    body{
    background: rgba(158, 158, 158 , 0.3);;
    }

    #cuadro{
    width: 50%;
    max-width: 500px;
    padding: 30px;
    border: 1px solid rgba(0, 0, 0, 0.2);
    background: white;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    }

    .titulo{
    text-align: center;    
    }

    input{
    display: block;
    padding: 10px;
    width: 100%;
    margin: 30px 0;
    font-size: 20px;
    }

    button[type="submit"]{
    background: #D3DAD6;
   border: 0.5 solid;
   color: white;
   opacity: 0.8;
   cursor: pointer;
   border-radius: 20px;
   margin-bottom: 0;
    }

    button[type="submit"]:hover {
    -webkit-box-shadow: 0 0px 15px 0.1px rgba(0, 0, 0, 0.7);
    box-shadow: 0 0px 15px 0.1px rgba(0, 0, 0, 0.7);
    -webkit-transform: scale(1.003);
    -ms-transform: scale(1.003);
    transform: scale(1.003); }
  

    button[type="submit"]:active{
    transform: scale(0.93);
    }

    button{
    padding: 10px;
    width: 30%;
    margin: 25px auto;
    font-size: 20px;
    padding: 10px;
    position: fixed;
    top: 67%;
    left: 35%;
    }
</style>
</head>
<body>
	<form method="post" action="../../UsuarioC" id="cuadro">
	<div class="titulo">Iniciar Sesion</div>
	<label>Usuario</label>
	<input name="usuario" type="text">
	<label>Contraseņa</label>
	<input name="password" type="password">
	<button type="submit" name="">Acceder</button>
	</form>
</body>
</html>