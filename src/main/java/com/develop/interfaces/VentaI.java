package com.develop.interfaces;

import java.util.List;

import com.develop.model.VentaM;

public interface VentaI {
	
	public String registroVenta(VentaM ventaM);
	
	public List<VentaM> listarVenta(); 
	
	public String pagoVenta();
	
}
