package com.develop.model;

import java.time.LocalDateTime;

public class VentaM {
	
	private int id_venta;
	private String fkusuario;
	private String nombreP;
	private int cantidadP;
	private double precioP;
	private double totalV;
	private LocalDateTime fechaRegistroVenta;
	
	public VentaM() {
		super();
	}
	
	public VentaM(int id_venta, String fkusuario, String nombreP, int cantidadP, double precioP, double totalV,
			LocalDateTime fechaRegistroVenta) {
		super();
		this.id_venta = id_venta;
		this.fkusuario = fkusuario;
		this.nombreP = nombreP;
		this.cantidadP = cantidadP;
		this.precioP = precioP;
		this.totalV = totalV;
		this.fechaRegistroVenta = fechaRegistroVenta;
	}
	public int getId_venta() {
		return id_venta;
	}
	public void setId_venta(int id_venta) {
		this.id_venta = id_venta;
	}
	public String getFkusuario() {
		return fkusuario;
	}
	public void setFkusuario(String fkusuario) {
		this.fkusuario = fkusuario;
	}
	public String getNombreP() {
		return nombreP;
	}
	public void setNombreP(String nombreP) {
		this.nombreP = nombreP;
	}
	public int getCantidadP() {
		return cantidadP;
	}
	public void setCantidadP(int cantidadP) {
		this.cantidadP = cantidadP;
	}
	public double getPrecioP() {
		return precioP;
	}
	public void setPrecioP(double precioP) {
		this.precioP = precioP;
	}
	public double getTotalV() {
		return totalV;
	}
	public void setTotalV(double totalV) {
		this.totalV = totalV;
	}
	public LocalDateTime getFechaRegistroVenta() {
		return fechaRegistroVenta;
	}
	public void setFechaRegistroVenta(LocalDateTime fechaRegistroVenta) {
		this.fechaRegistroVenta = fechaRegistroVenta;
	}

	
	
}
